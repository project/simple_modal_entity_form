<?php

/**
 * @file
 * Contains all the views hooks.
 */

/**
 * Implements hook_views_data().
 */
function simple_modal_entity_form_views_data() {
  $data = [];
  // Registers an entity area handler per entity type.
  foreach (\Drupal::entityTypeManager()->getDefinitions() as $entity_type_id => $entity_type) {
    if (!$entity_type->hasFormClasses()) {
      continue;
    }
    $tables = [];
    $tables[] = $entity_type->getDataTable() ?: $entity_type->getBaseTable();
    if ($entity_type->isRevisionable()) {
      $tables[] = $entity_type->getRevisionDataTable() ?: $entity_type->getRevisionTable();
    }
    foreach ($tables as $table) {
      $label = $entity_type->getLabel();
      $data[$table][$entity_type_id . '_modal_edit'] = [
        'title' => t('Link to edit @label (Modal)', ['@label' => $label]),
        'help' => t('Provide an link to edit the @label in a modal.', ['@label' => $label]),
        'area' => [
          'entity_type' => $entity_type_id,
          'id' => 'entity',
        ],
        'field' => [
          'id' => 'modal_entity_edit_field',
        ],
      ];
      $data[$table][$entity_type_id . '_modal_delete'] = [
        'title' => t('Link to delete @label (Modal)', ['@label' => $label]),
        'help' => t('Provide an link to delete the @label in a modal.', ['@label' => $label]),
        'area' => [
          'entity_type' => $entity_type_id,
          'id' => 'entity',
        ],
        'field' => [
          'id' => 'modal_entity_delete_field',
        ],
      ];
      $data[$table][$entity_type_id . '_modal_view'] = [
        'title' => t('Link to view @label (Modal)', ['@label' => $label]),
        'help' => t('Provide an link to view the @label in a modal.', ['@label' => $label]),
        'area' => [
          'entity_type' => $entity_type_id,
          'id' => 'entity',
        ],
        'field' => [
          'id' => 'modal_entity_view_field',
        ],
      ];
    }
  }
  return $data;
}
