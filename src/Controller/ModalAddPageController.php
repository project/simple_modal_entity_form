<?php

namespace Drupal\simple_modal_entity_form\Controller;

use Drupal\Core\Entity\Controller\EntityController;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Routing\RedirectDestinationInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Routing\UrlGeneratorInterface;

/**
 * Creates a modal version of the add page linking modal forms.
 */
class ModalAddPageController extends EntityController {

  /**
   * The destination service.
   *
   * @var \Drupal\Core\Routing\RedirectDestinationInterface
   */
  protected $destination;

  /**
   * Constructs a new ModalAddPageController.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation.
   * @param \Drupal\Core\Routing\UrlGeneratorInterface $url_generator
   *   The url generator.
   * @param \Drupal\Core\Routing\RedirectDestinationInterface $destination
   *   The destination service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityTypeBundleInfoInterface $entity_type_bundle_info, EntityRepositoryInterface $entity_repository, RendererInterface $renderer, TranslationInterface $string_translation, UrlGeneratorInterface $url_generator, RedirectDestinationInterface $destination) {
    parent::__construct($entity_type_manager, $entity_type_bundle_info, $entity_repository, $renderer, $string_translation, $url_generator);
    $this->destination = $destination;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('entity.repository'),
      $container->get('renderer'),
      $container->get('string_translation'),
      $container->get('url_generator'),
      $container->get('redirect.destination')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function addPage($entity_type_id) {
    $build = parent::addPage($entity_type_id);

    // Add a specific selector so the modal doesn't conflict with other modals.
    // This depends on https://www.drupal.org/project/drupal/issues/2741877
    // to land.
    $build['#modal_selector'] = '#modal_smef';
    $build['#attached']['library'][] = 'simple_modal_entity_form/simple_modal_entity_form.ajax';
    if (isset($build['#bundles'])) {
      foreach ($build['#bundles'] as $bundle => $item) {
        $build['#bundles'][$bundle]['add_link'] = Link::fromTextAndUrl($item['label'], Url::fromRoute('modal_entity_form.add', [
          'entity_type' => $entity_type_id,
          'bundle' => $bundle,
          'form_mode' => 'default',
        ], [
          'attributes' => [
            'class' => ['use-ajax'],
            'data-dialog-type' => 'modal',
            'data-dialog-options' => json_encode([
              'width' => '50%',
              'height' => '500px',
            ]),
          ],
          'query' => [
            'destination' => $this->destination->get() ?: Url::fromRoute('<current>')->toString(),
          ],
        ]));
      }
    }
    return $build;
  }

}
