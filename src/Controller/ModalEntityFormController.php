<?php

namespace Drupal\simple_modal_entity_form\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenDialogCommand;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityFormBuilderInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityTypeRepositoryInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for Modal entity form routes.
 */
class ModalEntityFormController extends ControllerBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity type repository.
   *
   * @var \Drupal\Core\Entity\EntityTypeRepositoryInterface
   */
  protected $entityTypeRepository;

  /**
   * The entity form builder.
   *
   * @var \Drupal\Core\Entity\EntityFormBuilderInterface
   */
  protected $entityFormBuilder;

  /**
   * The entity display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * The entity type bundle info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * Constructs the controller object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeRepositoryInterface $entity_type_repository
   *   The entity type repo.
   * @param \Drupal\Core\Entity\EntityFormBuilderInterface $entity_form_builder
   *   The entity form builder.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The entity display repository.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityTypeRepositoryInterface $entity_type_repository, EntityFormBuilderInterface $entity_form_builder, EntityDisplayRepositoryInterface $entity_display_repository, EntityTypeBundleInfoInterface $entity_type_bundle_info) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityTypeRepository = $entity_type_repository;
    $this->entityFormBuilder = $entity_form_builder;
    $this->entityDisplayRepository = $entity_display_repository;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_type.repository'),
      $container->get('entity.form_builder'),
      $container->get('entity_display.repository'),
      $container->get('entity_type.bundle.info')
    );
  }

  /**
   * Builds the response.
   *
   * @param string $entity_type
   *   The entity type.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity.
   * @param string $form_mode
   *   The form mode to use.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  public function editEntity(string $entity_type, ContentEntityInterface $entity, string $form_mode) {
    $response = new AjaxResponse();
    $form = $this->entityFormBuilder->getForm($entity, 'modal', ['modal_form_display' => EntityFormDisplay::collectRenderDisplay($entity, $form_mode)]);
    $form['#attached']['library'][] = 'simple_modal_entity_form/simple_modal_entity_form.ajax';
    $page_title = isset($form['#title']) ? $form['#title'] : $this->t('Edit @type', ['@type' => $entity->label()]);
    $response->addCommand(new OpenDialogCommand('#modal-entity-form', $page_title, $form, [
      'max-width' => '900px',
      'width' => '90%',
      'modal' => TRUE,
    ]));
    return $response;
  }

  /**
   * Builds the response.
   *
   * @param string $entity_type
   *   The entity type.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  public function deleteEntity(string $entity_type, ContentEntityInterface $entity) {
    $response = new AjaxResponse();
    $form = $this->entityFormBuilder->getForm($entity, 'delete');
    $form['#attached']['library'][] = 'simple_modal_entity_form/simple_modal_entity_form.ajax';
    $response->addCommand(new OpenModalDialogCommand(t('Delete @label', ['@label' => $entity->label()]), $form, ['width' => '800px']));
    return $response;
  }

  /**
   * Controller for the add entity form.
   *
   * @param string $entity_type
   *   The entity type.
   * @param string $bundle
   *   The bundle.
   * @param string $form_mode
   *   The form mode.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The ajax response.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *
   * @todo Upcast the entity type and bundle.
   */
  public function addEntity(string $entity_type, string $bundle, string $form_mode) {

    $response = new AjaxResponse();
    $entity_info = $this->entityTypeManager->getDefinition($entity_type);
    $form['#attached']['library'][] = 'simple_modal_entity_form/simple_modal_entity_form.ajax';
    $values = [];
    if ($bundle_key = $entity_info->getKey('bundle')) {
      if ($entity_info->getBundleEntityType()) {
        $values[$bundle_key] = $bundle;
      }
      $bundle_info = $this->entityTypeBundleInfo->getBundleInfo($entity_type);
      $label = $bundle_info[$bundle]['label'];
    }
    else {
      $label = $entity_info->getLabel();
    }
    $entity = $this->entityTypeManager->getStorage($entity_type)->create($values);
    $form = $this->entityFormBuilder->getForm($entity, 'modal', ['modal_form_display' => EntityFormDisplay::collectRenderDisplay($entity, $form_mode)]);
    $page_title = isset($form['#title']) ? $form['#title'] : $this->t('Create @type', ['@type' => $label]);

    $reflection = new \ReflectionClass(OpenModalDialogCommand::class);

    // For ckeditor plugins such as Media Library and Link to work properly
    // the patch in https://www.drupal.org/project/drupal/issues/2741877
    // needs to be applied.
    if ($reflection->getConstructor()->getNumberOfParameters() == 4) {
      $response->addCommand(new OpenModalDialogCommand($page_title, $form, ['width' => '1200px'], NULL));
    }
    else {
      $response->addCommand(new OpenModalDialogCommand($page_title, $form, ['width' => '1200px'], NULL, '#modal-smef'));
    }

    return $response;
  }

  /**
   * Access callback.
   *
   * @param string $entity_type
   *   The entity type.
   * @param string $bundle
   *   The bundle.
   * @param string $form_mode
   *   The form mode.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result
   */
  public function createEntityAccess($entity_type, $bundle, $form_mode, AccountInterface $account) {
    /** @var \Drupal\Core\Entity\EntityDisplayRepository $displayRepository */
    $displayRepository = $this->entityDisplayRepository;
    $display_modes = $displayRepository->getFormModeOptions($entity_type);
    if (!in_array($form_mode, array_keys($display_modes))) {
      return AccessResult::forbidden('No valid form display mode.');
    }
    return $this->entityTypeManager->getAccessControlHandler($entity_type)->createAccess($bundle, $account, [], TRUE);
  }

  /**
   * Access callback for editing entities.
   *
   * @param string $entity_type
   *   The entity type.
   * @param string $entity
   *   The entity id.
   * @param string $form_mode
   *   The form mode.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account.
   *
   * @return bool|\Drupal\Core\Access\AccessResultInterface
   *   The access result.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function editEntityAccess($entity_type, $entity, $form_mode, AccountInterface $account) {
    $entity = $this->entityTypeManager->getStorage($entity_type)->load($entity);
    /** @var \Drupal\Core\Entity\EntityDisplayRepository $displayRepository */
    $displayRepository = $this->entityDisplayRepository;
    $display_modes = $displayRepository->getFormModeOptionsByBundle($entity_type, $entity->bundle());
    if (!in_array($form_mode, array_keys($display_modes))) {
      return AccessResult::forbidden('No valid form display mode.');
    }
    return $entity->access('update', $account, TRUE);
  }

  /**
   * Access callback for deleting entities.
   *
   * @param string $entity_type
   *   The entity type.
   * @param string $entity
   *   The entity id.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user account.
   *
   * @return bool|\Drupal\Core\Access\AccessResultInterface
   *   The result.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function deleteEntityAccess($entity_type, $entity, AccountInterface $account) {
    $entity = $this->entityTypeManager->getStorage($entity_type)->load($entity);
    return $entity->access('delete', $account, TRUE);
  }

}
