<?php

namespace Drupal\simple_modal_entity_form\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\simple_modal_entity_form\Ajax\ModalEntityFormScrollTopCommand;

/**
 * Generic Handler for Modal forms.
 */
class ModalEntityForm extends ContentEntityForm {

  /**
   * Get form display.
   */
  public function getFormDisplay(FormStateInterface $form_state) {
    return $form_state->get('modal_form_display');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $form['#prefix'] = '<div id="modal-form">';
    $form['#suffix'] = '</div>';
    $form['messages'] = [
      '#weight' => -9999,
      '#type' => 'status_messages',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function actionsElement(array $form, FormStateInterface $form_state) {
    $element = parent::actionsElement($form, $form_state);
    $element['submit']['#ajax'] = [
      'callback' => '::ajaxFormSubmitHandler',
      'wrapper' => 'modal-form',
      'progress' => [
        'type' => 'fullscreen',
      ],
    ];
    return $element;
  }

  /**
   * Handle errors in the modal.
   *
   * @param array $form
   *   The form passed by reference.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response object.
   */
  public function ajaxFormSubmitHandler(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    if (!empty($form_state->getErrors())) {
      $response->addCommand(new ModalEntityFormScrollTopCommand());
      $response->addCommand(new ReplaceCommand('#modal-form', $form));
      return $response;
    }
    $this->submitForm($form, $form_state);
    $this->messenger()->addMessage(new TranslatableMarkup('Successfully saved @type %label.', [
      '@type' => $this->getEntity()->getEntityType()->getSingularLabel(),
      '%label' => $this->getEntity()->label(),
    ]));
    $destination = $this->getRequest()->query->get('destination');
    $url = Url::fromUserInput($destination);
    $query = $url->getOption('query');
    $query['smef_id'] = $this->getEntity()->id();
    $url->setOption('query', $query);
    $response->addCommand(new RedirectCommand($url->toString()));
    return $response;
  }

}
