<?php

namespace Drupal\simple_modal_entity_form\Plugin\views\field;

use Drupal\Core\Access\AccessManagerInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\views\Plugin\views\field\LinkBase;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A base class for modal operations.
 */
abstract class ModalEntityOperationBase extends LinkBase {

  /**
   * The entity display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('access_manager'),
      $container->get('entity_type.manager'),
      $container->get('entity.repository'),
      $container->get('language_manager'),
      $container->get('entity_display.repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, AccessManagerInterface $access_manager, EntityTypeManagerInterface $entity_type_manager = NULL, EntityRepositoryInterface $entity_repository = NULL, LanguageManagerInterface $language_manager = NULL, EntityDisplayRepositoryInterface $entity_display_repository) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $access_manager, $entity_type_manager, $entity_repository, $language_manager);
    $this->entityDisplayRepository = $entity_display_repository;
  }


  /**
   * {@inheritdoc}
   */
  public function renderText($alter) {
    if (isset($alter['url'])) {
      $options = $alter['url']->getOptions();
      $options['attributes']['class'][] = 'use-ajax';
      $options['attributes']['data-dialog-type'] = 'modal';
      $dialog_options = [];
      if ($this->options['width']) {
        $dialog_options['width'] = $this->options['width'];
      }
      if ($this->options['height']) {
        $dialog_options['height'] = $this->options['height'];
      }
      if ($dialog_options) {
        $options['attributes']['data-dialog-options'] = json_encode($dialog_options);
      }
      $alter['url']->setOptions($options);
    }
    return parent::renderText($alter);
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $row) {
    $build = parent::render($row);
    $build['#attached']['library'][] = 'simple_modal_entity_form/simple_modal_entity_form.ajax';
    $build['#cache']['contexts'] = Cache::mergeTags($build['#cache']['contexts'], ['url']);
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    $form['width'] = [
      '#type' => 'number',
      '#title' => $this->t('Width'),
      '#default_value' => $this->options['width'],
    ];
    $form['height'] = [
      '#type' => 'number',
      '#title' => $this->t('Height'),
      '#default_value' => $this->options['height'],
    ];
    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['width'] = ['default' => 800];
    $options['height'] = ['default' => NULL];
    return $options;
  }

}
