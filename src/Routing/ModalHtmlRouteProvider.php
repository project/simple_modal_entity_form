<?php

namespace Drupal\simple_modal_entity_form\Routing;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider;
use Drupal\simple_modal_entity_form\Controller\ModalAddPageController;
use Symfony\Component\Routing\Route;

/**
 * Adds extra routing for expload UI.
 */
class ModalHtmlRouteProvider extends DefaultHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $collection = parent::getRoutes($entity_type);
    $entity_type_id = $entity_type->id();
    if ($add_page_modal = $this->getAddPageModal($entity_type)) {
      $collection->add("entity.{$entity_type_id}.add_page_modal", $add_page_modal);
    }
    return $collection;
  }

  /**
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *
   * @return \Symfony\Component\Routing\Route|boolean
   */
  protected function getAddPageModal(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('add-page-modal') && $entity_type->getKey('bundle')) {
      $route = new Route($entity_type->getLinkTemplate('add-page-modal'));
      $route->setDefault('_controller', ModalAddPageController::class . '::addPage');
      $route->setDefault('_title_callback', ModalAddPageController::class . '::addTitle');
      $route->setDefault('entity_type_id', $entity_type->id());
      $route->setRequirement('_entity_create_any_access', $entity_type->id());
      return $route;
    }
    return FALSE;
  }

}
